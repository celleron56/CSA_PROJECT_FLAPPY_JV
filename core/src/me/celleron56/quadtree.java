package me.celleron56;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class quadtree{
    int value=0;
    int size=64;
    int[] qpos = {0,0};
    quadtree[] bases;
    public quadtree(int size){
        size=size;
        bases =new quadtree[]{null,null,null,null};

    }
    int set(int[] pos ,int valuex){
        if (pos[0]>=size){
            return 0;
        }
        if (pos[1]>=size){
            return 0;
        }

        if (size<2){
            this.value =  valuex;
            return value;
        }
        int key= Math.min(Math.max(0,pos[0] / (size/2)),1) *2;
        int xo=key<2 ? 0:1;
        int yo=(pos[1] / (size/2));
        key+=(pos[1]<(size/2))?0:1;
        quadtree index = bases[key];
        if (index == null) {
            bases[key] = new quadtree((int) (size*0.5) );

        }else{
            this.value-=bases[key].value;

        }
        bases[key].size=this.size/2;
        bases[key].qpos= new int[]{this.qpos[0] + ((size / 2) * xo), this.qpos[1] + ((size / 2) * yo)};
        int nv=bases[key].set(new int[]{(int) (pos[0]%(size/2)), (int) (pos[1]%(size/2))},valuex);
        this.value+=nv;
        //System.out.println("value :" + nv);
        /*if (this.value==0 || this.value==2000){
            bases = new quadtree[]{null,null,null,null};
        }*/
        int t=-1;
        int o=1;
        for (quadtree i : bases){
            if (i!=null){
                if (t==-1){
                    t=i.value;
                }
                if (t!=i.value){
                    o=0;
                }
            }else{
                o=0;
            }
        }
        if (o==1) {
            value=t;
            bases = new quadtree[]{null, null, null, null};
        }
        if (this.size ==64) {
            return (int) ((this.value / 4) * 0.0005);
        }else{
            return (int) ((this.value / 4));
        }

    }
    java.util.ArrayList<int[]> get_all_in_region(int[] pos1, int[] pos2) {
        java.util.ArrayList<int[]> rv = new java.util.ArrayList<>();
        if ((bases[0]==null && bases[1] ==null && bases[2]==null && bases[3]==null)||(size<2)) {
            rv.add(new int[]{this.qpos[0], this.qpos[1], this.qpos[0] + this.size, this.qpos[1] + this.size,this.value});
            return rv;
        }
        int index = 0;
        for (quadtree i : bases) {

            int sd2 = (this.size / 2);
            int i_x = ((index < 2) ? 0 : 1) * sd2;
            int i_y = (index % 2) * sd2;
            int[] tqtp = new int[]{qpos[0] + i_x, qpos[1] + i_y, qpos[0] + i_x + sd2, qpos[1] + i_y + sd2,value};
            if (this.collides(new int[]{pos1[0], pos1[1], pos2[0], pos2[1]}, tqtp)) {
                if (i == null) {
                    rv.add(tqtp);
                } else {
                    for(int[] ix:i.get_all_in_region(pos1,pos2)){
                        rv.add(ix);
                    }
                }


                index++;
            }
        }
        return rv;
    }
    boolean collides(int[] r1,int[] r2){
        if (r1[0]>r2[2] || r2[0] >r1[2]){
            return false;
        }
        if (r1[1]>r2[3] || r2[1] >r1[3]){
            return false;
        }
        return true;


    }
    int get(int[] pos ){
        System.out.println(size);
        if (size<2){
            return value;
        }
        int key= Math.max(Math.min(0,pos[1] / (size/2)),1) *2;
        key=key + (pos[1] / (size/2));
        quadtree index = bases[key];
        if (index == null) {
            return value;
        }
        int nv=bases[key].get(new int[]{(int) (pos[0]%(size/2)), (int) (pos[1]%(size/2))});

        return nv;


    }

}