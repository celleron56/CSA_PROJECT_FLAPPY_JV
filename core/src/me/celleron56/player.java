package me.celleron56;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class player implements  gameobject {
    double[] pos;
    double ac;
    float squish=0;

    @Override
    public void render(ShapeRenderer shapes, float ft) {
        //pepe the frog since my ninja box reminded me of him
        //body


        shapes.setColor(0.3f,0.56f,0.2f,0.9f);
        shapes.ellipse((int) pos[0],(int) pos[1],20-(squish*19),18);
        //eyes
        for (int i = 0; i < 2; i++) {

            shapes.setColor(0.3f,0.5f,0.2f,0.9f);
            shapes.rect((int) pos[0] + 11 + (i*6)-((11+(i*6))*squish), (int) (pos[1] + 12 + (ac * 0.5)), 5-(4*squish), 4);

            shapes.setColor(0.9f, 0.8f, 0.95f, 0.9f);
            shapes.rect((int) pos[0] + 12+ (i*6)-((12+(i*6))*squish), (int) (pos[1] + 12 + (ac * 0.5)), 3-(3*squish), 3);
            shapes.setColor(0.1f, 0.05f, 0.1f, 0.9f);
            shapes.rect((int) pos[0] + 13+ (i*6)-((13+(i*6))*squish), (int) (pos[1] + 13 + (ac * 0.54)), 1, 1);
        }
        shapes.setColor(0.6f,0.3f,0.1f,0.9f);
        shapes.rect((int) pos[0]+10-(10*squish) ,(int) (pos[1]+8 +(ac*0.5)),10-(9*squish),2);
        shapes.rect((int) pos[0]+(9-(9*squish)) ,(int) (pos[1]+9 +(ac*0.45)),1,2);
        shapes.rect((int) pos[0]+5-(5*squish) ,(int) (pos[1]+10 +(ac*0.4)),4-(3*squish),2);
        shapes.rect((int) pos[0]+4 -(4*squish),(int) (pos[1]+9 +(ac*0.4)),1,2);
        if ((pos[1]>0 || ac>0)){
            pos[1]+=ac*ft*10;
        }
        if (ac>-4){
            ac-=0.5;
        }
    }

    public Object rs(String signal){
        if (signal=="GPY"){
            return (int) pos[1];
        }
        if (signal=="RETURNP"){
            return this;
        }
        return null;
    }

    @Override
    public void __init__(int[] pos) {
        this.pos = new double[]{(double) pos[0],(double) pos[1]};

    }

    @Override
    public void input(String key) {
        if (key=="Space" && ac >-5){
            ac=9;
            pos[1]=pos[1]%640;
        }

    }
}
