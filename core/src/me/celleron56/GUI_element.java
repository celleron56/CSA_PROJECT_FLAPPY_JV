package me.celleron56;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.HashMap;

public abstract class GUI_element  implements  gameobject{
    int hidden;
    int[] pos;
    int[] size;
    void onclick(){};
    void send_sig(Object signal){
        this.nsig=signal;
    }
    void on_signal(Object signal){

    }


    void on_mouse_motion(int[] pos){};
    String name;
    Object nsig;

    void handle(int[] mousestate, HashMap<String, Object> io){
        if (io.containsKey(this.name)==false){
            io.put(this.name,null);
        }

        nsig=null;
        Object psig = io.get(this.name);
        if( psig!=null){
            on_signal(psig);
        }

        if ((mousestate[0] > pos[0]) && (mousestate[0]<(pos[0]+size[0]))){
            if((mousestate[1] > pos[1]) && (mousestate[1]<(pos[1]+size[1]))){
                on_mouse_motion(mousestate);//no need to remove 3rd value
                if (mousestate[2]>0){
                    onclick();
                }
            }
        }


    }

}
