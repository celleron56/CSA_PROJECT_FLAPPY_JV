package me.celleron56;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class button extends  GUI_element  {
    BitmapFont font = new BitmapFont();
    int hidden=0;
    int score=0;
    int mo=0;
    Batch fb = new SpriteBatch();
    void on_mouse_motion(int[] pos){
        mo=1;
    };
    void onclick(){
        hidden=1;
    };
    @Override
    public void render(ShapeRenderer shapes, float ft) {


        if (hidden==0) {
            shapes.setColor(0f, 0f, (float) mo, 0.4f);
            mo = 0;
            for (int i = 0; i < size[1] / 2; i++) {
                shapes.rect(300 - (size[0] / 2), 110 + (i * 2), size[0], 1);
            }
        }


        shapes.end();
        fb.begin();
        if (hidden==0) {
            font.getData().setScale(10);

            font.draw(fb, "START", 320 - (size[0] / 2), 640 - (pos[1] + 190));
        }
        font.getData().setScale(1);
        font.draw(fb,"Score: "+String.valueOf(score),10,470);

        fb.end();


        shapes.begin(ShapeRenderer.ShapeType.Filled);



    }

    @Override
    public void __init__(int[] pos) {
        this.size=new int[]{500,120};
        this.pos=new int[]{300-(size[0]/2),110+size[1]};


    }

    @Override
    public Object rs(String signal) {
        if (signal=="RTS") {
        return hidden;
        }
        if (signal=="DED") {
            hidden=0;
        }
        if (signal=="UPD_SCORE_HELPER"){
            return this;
        }
        return null;
    }

    @Override
    public void input(String key) {


    }
}
